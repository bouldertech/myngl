(function ($) {
  
  function runCommands(commands) {   
    var element_settings = {event: 'fake_events', url: ''};
    var element = $('');
    var ajax_comments = new Drupal.ajax('ajax_comments', element, element_settings);
    
    for (var i in commands) {
      var command = commands[i],
          command_name = command['command'];
      
      if (command_name && ajax_comments.commands[command_name]) {        
        ajax_comments.commands[command_name](ajax_comments, command, 200);
      }
    }
  }
  
  Drupal.Nodejs.callbacks.ajaxCommentsInsert = {
    callback: function (message) {
      if (typeof message.cid != 'undefined') {
      $.get('/ajax_comments_nodejs/insert/' + message.cid, function (data) {
        runCommands(data);

        // Auto-scroll to newest comment
        $('#comments > DIV').animate({ scrollTop: $('#comments > DIV')[0].scrollHeight }, 'fast');

        setTimeout(function() {
	      $('#messages').fadeOut('slow', function () {
		    $('#messages').remove();
		  });
		}, 2000);
      });
      }
    }
  };
  
  Drupal.Nodejs.callbacks.ajaxCommentsUpdate = {
    callback: function (message) {
      if (typeof message.cid != 'undefined') {
      $.get('/ajax_comments_nodejs/update/' + message.cid, function (data) {
        runCommands(data);
      });
      }
    }
  };
  
  Drupal.Nodejs.callbacks.ajaxCommentsDelete = {
    callback: function (message) {
      runCommands(message.commands);
    }
  };
  
})(jQuery);
