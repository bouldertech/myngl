<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */


function themingl_preprocess_page(&$vars) {

  global $user;

  $users = array();
  if(variable_get('drupalchat_enable_chatroom', 1) == 1) {
    $users['c-0'] = array('name' => t('POV Wall'), 'status' => '1');
  }
  if (isset($vars['node'])) {
  $vars['theme_hook_suggestion'] = 'page__'.$vars['node']->type; //
}

}

function themingl_preprocess_views_slideshow_jcarousel_pager(&$vars) {
_views_slideshow_jcarousel_preprocess_pager($vars);
}

function themingl_preprocess_views_slideshow_jcarousel_pager_item(&$vars) {
_views_slideshow_jcarousel_preprocess_pager_item($vars);
}

function themingl_preprocess_html(&$variables, $hook) {
  if ( isset($_GET['ajax']) && $_GET['ajax'] == 1 ) {
    $variables['theme_hook_suggestions'][] = 'html__ajax';
  }  
}
function profile_custom_submit($form, &$form_state) {
      $form_state['redirect'] = '/';
}

