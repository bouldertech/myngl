<?php $left = strpos($attributes, 'comment-by-viewer') !== false ? true : false; ?>
<table<?php print $attributes; ?> style="margin-bottom:0;">
	<tr>
<?php if ($left) { ?>
		<td style="padding-bottom:0;">
		<table style="margin-bottom:0;">
			<tr>
		<td style="padding:0;width:66px;vertical-align:bottom;"><?php print $picture; ?></td>
		<td style="vertical-align:bottom;padding:0 0 0 20px;">
			<div style="position:relative;">
			<img src="/sites/all/themes/themingl/images/gray-bubble.gif" alt="" style="width:10px;position:absolute;left:-5px;bottom:9px;" />
			<div class="comment-body">
				<div<?php print $content_attributes; ?>>
					<?php
						hide($content['links']);
						print render($content);
					?>
				</div>
				<footer class="comment-submitted">
					<?php /******
						print t('Submitted by !username on !datetime',
						array('!username' => $author, '!datetime' => '<time datetime="' . $datetime . '">' . $created . '</time>'));
						******/ ?>
				</footer>
				<?php if ($signature) { ?>
				<div class="user-signature"><?php print $signature ?></div>
				<?php } ?>
				<?php if (!empty($content['links'])): ?>
				<nav class="links comment-links clearfix"><?php print render($content['links']); ?></nav>
				<?php endif; ?>
			</div>
			</div>
		</td>
			</tr>
		</table>
		</td>
<?php } else { ?>
		<td>
		<table style="margin-bottom:0;">
			<tr>
		<td style="vertical-align:bottom;padding:0 20px 0 0;position:relative;">
			<div style="position:relative;">
			<img src="/sites/all/themes/themingl/images/yellow-bubble.gif" alt="" style="width:10px;position:absolute;right:-5px;bottom:9px;" />
			<div class="comment-body" style="float:right;">
				<div<?php print $content_attributes; ?>>
					<?php
						hide($content['links']);
						print render($content);
					?>
				</div>
				<footer class="comment-submitted">
					<?php /******
						print t('Submitted by !username on !datetime',
						array('!username' => $author, '!datetime' => '<time datetime="' . $datetime . '">' . $created . '</time>'));
						******/ ?>
				</footer>
				<?php if ($signature) { ?>
				<div class="user-signature"><?php print $signature ?></div>
				<?php } ?>
				<?php if (!empty($content['links'])): ?>
				<nav class="links comment-links clearfix"><?php print render($content['links']); ?></nav>
				<?php endif; ?>
			</div>
			<div style="clear:both;height:0;">&nbsp;</div>
			</div>
		</td>
		<td style="padding:0;width:66px;vertical-align:bottom;"><?php print $picture; ?></td>
			</tr>
		</table>
		</td>
<?php } ?>
	</tr>
</table>