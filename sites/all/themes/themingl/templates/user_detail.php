<?php
include_once '../../../../../includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$get_param = $_GET["type"];
$arguments = Array();
if(!empty($get_param)){
    array_push($arguments, $get_param);
}

$view = views_get_view('main_room_detail');
print $view->execute_display('Block', $arguments);
?>
